FROM postgres:17

LABEL PG="postgres"
RUN DEBIAN_FRONTEND=noninteractive apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -q -y apt
RUN DEBIAN_FRONTEND=noninteractive apt update && apt -q -y full-upgrade

ARG POSTGRES_PASSWORD
ARG POSTGRES_HOST_AUTH_METHOD
ARG PG_DB
ARG PG_USER
ARG PG_PASS

COPY ./docker-entrypoint-initdb.d/ /docker-entrypoint-initdb.d/
RUN chmod 0755 /docker-entrypoint-initdb.d/*
RUN ls -la /docker-entrypoint-initdb.d/

CMD [ "postgres",  "-c",  "config_file=/etc/postgresql.conf" ]

