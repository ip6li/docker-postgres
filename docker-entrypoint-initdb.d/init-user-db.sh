#!/usr/bin/env bash
set -e

echo "init-user-db.sh called"

if [ -z "${PG_USER}" ]; then
  echo "PG_USER not defined in .env, so we do not prepopulate database. This ist not an error."
  exit 0
fi

TMP_USER=()
TMP_DB=()
TMP_PASS=()
for i in ${PG_USER}; do
  TMP_USER+=(${i})
done
for i in ${PG_DB}; do
  TMP_DB+=(${i})
done
for i in ${PG_PASS}; do
  TMP_PASS+=(${i})
done

count=0
for i in ${TMP_USER[@]}; do

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER $i WITH PASSWORD '${TMP_PASS[$count]}';
	CREATE DATABASE ${TMP_DB[$count]};
	GRANT ALL PRIVILEGES ON DATABASE ${TMP_DB[$count]} TO $i;
        ALTER DATABASE ${TMP_DB[$count]} OWNER TO $i;
EOSQL

  let count=$count+1
done

