# Custom Config

See docker-compose.override.yml.example. You must have a docker-compose.override.yml to expose PostgreSQL port.

# Postgres Command

docker run -it --rm --network br2 postgres psql -h 192.168.1.247 -U postgres

# Password

ALTER ROLE username WITH PASSWORD 'password';

# Backup

Consider to set following variables in .env:

```
TARGET_DIR=<where to save your valuable data>
GPG_KEYID=<A PGP public key>
MAX_DAYS=<Backup retension in days>
```

Start backup with

```
./psql-backup
```

# Restore

```
xz -d < /backup/container/postgres/pg_backup.bak.20220821.123454.xz | docker compose exec -T -u postgres postgres psql
```

...and test your backups!

# Postgres User

# TLS

To set up PKI, you need following commands:

```
git submodule init
git submodule update
```

It needs a sudo to set permissions of generated certificate so it is accepted by Postgres.

run *mk-demo-cert.sh* to get a certificate set.

Please set up pg_hba.conf for client cert auth.

Check out TLS connection with

```
echo | openssl s_client -connect 172.17.0.1:5432 -starttls postgres -verifyCAfile pg-pki/ca.crt
```

