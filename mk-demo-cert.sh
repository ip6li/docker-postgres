#!/usr/bin/env bash

PG_HOSTNAME="pg.example.com"

cd pg-pki || exit 1

cp ../.env.pki .env

rm -rf demoCA "${CLIENT}.*" "${PG_HOSTNAME}.*"

./gen-ca 'My Postgres CA'
./mk-csr --server ${PG_HOSTNAME}
./sign --server ${PG_HOSTNAME}.csr
sudo chown 999:999 ${PG_HOSTNAME}.key ${PG_HOSTNAME}.crt

CLIENT="DemoClient"
./mk-csr --client "${CLIENT}"
./sign --client "${CLIENT}.csr"

